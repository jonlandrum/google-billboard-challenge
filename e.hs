import Data.Number.CReal

factorial n = if n < 2 then 1 else n * factorial (n - 1)

taylor n = if n < 2 then 1 else (1 / factorial n) + taylor (n - 1)

main::IO()

main = putStrLn(showCReal 1000 (taylor 100))
